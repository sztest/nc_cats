-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore
    = minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

minetest.register_ore({
		name = modname .. ":ore",
		ore_type = "scatter",
		ore = modname .. ":ore",
		wherein = "nc_terrain:stone",
		random_factor = 0,
		noise_params = {
			offset = 0,
			scale = 4,
			spread = {x = 20, y = 5, z = 20},
			seed = 61782,
			octaves = 3,
			persist = 0.5,
			flags = "eased",
		},
		noise_threshold = -1,
		y_max = 31000,
		y_min = -31000,
		clust_num_ores = 1,
		clust_size = 1,
		clust_scarcity = 8 * 8 * 8 * 4
	})

-- Opposite of lode ore, cat ore MUST be exposed to air
-- so only caves are worth searching
local c_ore = minetest.get_content_id(modname .. ":ore")
local c_stone = minetest.get_content_id("nc_terrain:stone")
local c_air = minetest.get_content_id("air")
nodecore.register_mapgen_shared({
		label = "cat ore exposure",
		func = function(minp, maxp, area, data)
			local ai = area.index
			for z = minp.z, maxp.z do
				for y = minp.y, maxp.y do
					local offs = ai(area, 0, y, z)
					for x = minp.x, maxp.x do
						local i = offs + x
						if data[i] == c_ore then
							if x == minp.x
							or x == maxp.x
							or y == minp.y
							or y == maxp.y
							or z == minp.z
							or z == maxp.z
							or (data[i - 1] ~= c_air
								and data[i + 1] ~= c_air
								and data[i - area.ystride] ~= c_air
								and data[i + area.ystride] ~= c_air
								and data[i - area.zstride] ~= c_air
								and data[i + area.zstride] ~= c_air)
							then data[i] = c_stone
						end
					end
				end
			end
		end
	end
})
