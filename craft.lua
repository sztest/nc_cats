-- LUALOCALS < ---------------------------------------------------------
local math, minetest, nodecore
    = math, minetest, nodecore
local math_random
    = math.random
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local myapi = _G[modname]

nodecore.register_soaking_abm({
		label = "cat mature",
		fieldname = "categg",
		nodenames = {modname .. ":cobble"},
		interval = 10,
		arealoaded = 1,
		soakrate = function(pos)
			return #nodecore.find_nodes_around(pos, "group:lava")
		end,
		soakcheck = function(data, pos)
			if data.total < 2000 then return end
			nodecore.sound_play(modname .. "_mew", {pos = pos})
			nodecore.set_loud(pos, {name = modname .. ":egg"})
			return nodecore.witness(pos, "cat mature")
		end
	})

local function reghatch(suff, cracky)
	return nodecore.register_craft({
			label = "hatch cat",
			action = "pummel",
			toolgroups = {cracky = cracky},
			indexkeys = {modname .. ":egg" .. suff},
			nodes = {
				{match = modname .. ":egg" .. suff}
			},
			items = {
				{name = "nc_stonework:chip", count = 4, scatter = 5}
			},
			itemscatter = 5,
			after = function(pos, data)
				return myapi.makecat(pos, nil, data.crafter)
			end
		})
end
reghatch("_loose", 4)
reghatch("", 6)

local function regprill(suff, cracky)
	return nodecore.register_craft({
			label = "extract cat prill",
			action = "pummel",
			toolgroups = {cracky = cracky},
			indexkeys = {modname .. ":cobble" .. suff},
			nodes = {
				{
					match = modname .. ":cobble" .. suff,
					replace = "nc_terrain:gravel"
				}
			},
			items = {
				{name = modname .. ":prill", scatter = 5},
				{name = "nc_stonework:chip", count = 2, scatter = 5}
			},
			itemscatter = 5,
			after = function(pos)
				if math_random(1, 2) ~= 1 then return end
				return nodecore.item_eject(pos, modname .. ":prill", 5)
			end
		})
end
regprill("_loose", 4)
regprill("", 6)

nodecore.register_craft({
		label = "cat renew",
		action = "pummel",
		toolgroups = {thumpy = 2},
		normal = {y = 1},
		indexkeys = {modname .. ":prill"},
		nodes = {
			{
				match = modname .. ":prill",
				replace = "air"
			},
			{
				y = -1,
				match = "nc_lux:cobble8",
				replace = modname .. ":cobble"
			}
		}
	})

nodecore.register_soaking_aism({
		label = "cat eggcorn",
		fieldname = "catcorn",
		itemnames = {"nc_tree:eggcorn"},
		interval = 10,
		arealoaded = 1,
		soakrate = function(stack, ctx)
			if stack:get_count() ~= 100 then return false end
			local pos = ctx.pos or ctx.player and ctx.player:get_pos()
			return nodecore.lux_soak_rate(pos)
		end,
		soakcheck = function(data, stack)
			if data.total < 5000 then return data.total, stack end
			return nil, modname .. ":prill"
		end
	})

nodecore.register_craft({
		label = "compress cat fluff",
		action = "pummel",
		toolgroups = {thumpy = 1},
		indexkeys = {modname .. ":prill"},
		nodes = {
			{
				match = {name = modname .. ":prill", count = 8},
				replace = modname .. ":packed_loose"
			}
		}
	})
nodecore.register_craft({
		label = "chop cat fluff",
		action = "pummel",
		toolgroups = {choppy = 1},
		nodes = {
			{match = {
					groups = {[modname .. "_packed_fluff"] = true}
			}, replace = "air"}
		},
		items = {{name = modname .. ":prill 2", count = 4, scatter = 5}}
	})
