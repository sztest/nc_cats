Adorable cats for NodeCore!

They come in 7 different colors and have almost a hundred unique names.  They meow when you pick them up, purr when you pet them, (sometimes cough up hairballs) but for the most part, they sit still and contentedly watch you work, occasionally reminding you of their presence.  These perfectly cubic indestructible cats will fit nicely into any build, even serving as load-bearing structural members, though beware that future updates may bring different, and possibly less desirable, behaviors.

If you want cats in your world, this mod will add the necessary materials to mapgen, but you will need to find them and figure out how to make the actual cats yourself.  The in-game Discovery system provides some guidance, and here are a few extra clues:

- Incubating cats love immersion in intense heat.
- Cat prills can only be forced into cobble that is energetic enough.
- There is a "skyblock" recipe for cat prills for situations where finding ore is impossible.  It requires quite a lot of seed material, which needs to accumulate a lot of energy.

***N.B. Not all mechanics are final.***  Future versions should retain compatibility, but all items, behaviors, and mapgen parameters may be subject to change, including changes that may not match already-generated map areas.

Cats may contain hidden features, and are processed in a facility that also processes hidden features.