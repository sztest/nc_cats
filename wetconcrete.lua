-- LUALOCALS < ---------------------------------------------------------
local math, minetest, nodecore, string
    = math, minetest, nodecore, string
local math_random, string_format
    = math.random, string.format
-- LUALOCALS > ---------------------------------------------------------

if not nodecore.concrete_wet_node then return end

local modname = minetest.get_current_modname()

nodecore.register_soaking_abm({
		label = "cat wet concrete",
		fieldname = "catwet",
		nodenames = {"group:" .. modname .. "_cat"},
		interval = 5,
		arealoaded = 1,
		soakrate = function() return 1 end,
		soakcheck = function(data, pos)
			if data.total < 60 then return end
			local below = {x = pos.x, y = pos.y - 1, z = pos.z}
			local found = nodecore.find_nodes_around(below,
				{"group:concrete_powder"}, {1, 0, 1})
			if #found > 0 then
				local p = found[math_random(1, #found)]
				local n = minetest.get_node(p)
				if nodecore.concrete_wet_node(p, n) then
					nodecore.log("action", string_format("%s at %s wets %s at %s",
							minetest.get_node(pos).name, minetest.pos_to_string(pos),
							n.name, minetest.pos_to_string(p)))
				end
			end
			return false
		end
	})
