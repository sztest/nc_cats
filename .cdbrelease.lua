-- LUALOCALS < ---------------------------------------------------------
local math, tonumber
    = math, tonumber
local math_floor
    = math.floor
-- LUALOCALS > ---------------------------------------------------------

-- luacheck: push
-- luacheck: globals config readtext readbinary

readtext = readtext or function() end
readbinary = readbinary or function() end

local stamp = tonumber("$Format:%at$")
if not stamp then return end
stamp = math_floor((stamp - 1540612800) / 60)
stamp = ("00000000" .. stamp):sub(-8)

return {
	user = "Warr1024",
	pkg = "nc_cats",
	type = "mod",
	dev_state = "ACTIVELY_DEVELOPED",
	version = stamp .. "-$Format:%h$",
	title = "NodeCore Cats",
	short_description = "Add adorable cats to NodeCore",
	tags = {"decorative", "mapgen", "mobs"},
	content_warnings = {},
	license = "MIT",
	media_license = "MIT",
	long_description = readtext('README.md'),
	repo = "https://gitlab.com/sztest/nc_cats",
	issue_tracker = "https://discord.gg/NNYeF6f",
	maintainers = {"Warr1024"},
	screenshots = {readbinary('.cdb1.webp'), readbinary('.cdb2.webp')}
}

-- luacheck: pop
